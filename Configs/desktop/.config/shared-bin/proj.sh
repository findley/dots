#!/bin/bash

find ~/workspace -mindepth 1 -maxdepth 1 -type d & find ~/workspace/muse -mindepth 1 -maxdepth 1 -type d | fzf --bind=ctrl-j:down,ctrl-l:up
