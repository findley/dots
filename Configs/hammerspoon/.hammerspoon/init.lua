local function runOrRaise(appName)
    local app = hs.application.get(appName)

    if app then
        local windowFilter = hs.window.filter.new(false):setAppFilter(appName)
        local windows = windowFilter:getWindows()

        local focusedWindow = hs.window.focusedWindow()
        local focusedApp = focusedWindow and focusedWindow:application()
        if focusedApp and focusedApp:name() == appName and #windows > 1 then
            hs.eventtap.keyStroke({"cmd"}, "`")
        else
            -- If app is not focused, bring it to the front
            hs.application.launchOrFocus(appName)
        end
    else
        -- If the app isn't running, launch it
        hs.application.launchOrFocus(appName)
    end
end

hs.hotkey.bind({"alt"}, "a", function() runOrRaise("Ghostty") end)
hs.hotkey.bind({"alt"}, "s", function() runOrRaise("Chromium") end)
