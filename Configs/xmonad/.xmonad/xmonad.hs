--
-- xmonad example config file.
--
-- A template showing all available configuration hooks,
-- and how to override the defaults in your own xmonad.hs conf file.
--
-- Normally, you'd only override those defaults you care about.
--
import XMonad
import XMonad.Config (def)
import XMonad.Layout.Fullscreen
    ( fullscreenEventHook, fullscreenManageHook, fullscreenSupport, fullscreenFull )
import Data.Monoid ()
import System.Exit
import XMonad.Util.SpawnOnce ( spawnOnce )
import XMonad.Util.Run
import Graphics.X11.ExtraTypes.XF86 (xF86XK_AudioLowerVolume, xF86XK_AudioRaiseVolume, xF86XK_AudioMute, xF86XK_MonBrightnessDown, xF86XK_MonBrightnessUp, xF86XK_AudioPlay, xF86XK_AudioPrev, xF86XK_AudioNext)
import XMonad.Hooks.EwmhDesktops ( ewmh )
import Control.Monad ( join, when )
import XMonad.Layout.MultiToggle
import XMonad.Layout.MultiToggle.Instances
import XMonad.Layout.NoBorders
import XMonad.Layout.WindowNavigation
import XMonad.Layout.IndependentScreens
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.DynamicProperty
import XMonad.Hooks.ManageDocks
    ( avoidStruts, docks, manageDocks, Direction2D(D, L, R, U) )
import XMonad.Hooks.ManageHelpers ( doFullFloat, isFullscreen )
import XMonad.Actions.Warp
import XMonad.Layout.Spacing ( spacingRaw, Border(Border), toggleScreenSpacingEnabled, toggleWindowSpacingEnabled )
import XMonad.Layout.Gaps
    ( Direction2D(D, L, R, U),
      gaps,
      setGaps,
      GapMessage(DecGap, ToggleGaps, IncGap) )

import qualified XMonad.StackSet as W
import qualified Data.Map        as M
import Data.Maybe (maybeToList)
-- The preferred terminal program, which is used in a binding below and by
-- certain contrib modules.
--
myTerminal = "alacritty"

-- Whether focus follows the mouse pointer.
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = True

-- Whether clicking on a window to focus also passes the click to the window
myClickJustFocuses :: Bool
myClickJustFocuses = False

-- Width of the window border in pixels.
--
myBorderWidth   = 0

-- modMask lets you specify which modkey you want to use. The default
-- is mod1Mask ("left alt").  You may also consider using mod3Mask
-- ("right alt"), which does not conflict with emacs keybindings. The
-- "windows key" is usually mod4Mask.
--
myModMask       = mod1Mask

-- The default number of workspaces (virtual screens) and their names.
-- By default we use numeric strings, but any string may be used as a
-- workspace name. The number of workspaces is determined by the length
-- of this list.
--
-- A tagging example:
--
-- > workspaces = ["web", "irc", "code" ] ++ map show [4..9]
--
-- myWorkspaces = ["web", "front", "back", "misc", "chat"]
myWorkspaces = ["1", "2", "3", "4", "\xF1477"]

myWorkspaceKeys = [xK_1, xK_2, xK_3, xK_4, xK_0]

-- Border colors for unfocused and focused windows, respectively.
--
myNormalBorderColor  = "#444444"
myFocusedBorderColor = "#883333"

addNETSupported :: Atom -> X ()
addNETSupported x   = withDisplay $ \dpy -> do
    r               <- asks theRoot
    a_NET_SUPPORTED <- getAtom "_NET_SUPPORTED"
    a               <- getAtom "ATOM"
    liftIO $ do
       sup <- (join . maybeToList) <$> getWindowProperty32 dpy a_NET_SUPPORTED r
       when (fromIntegral x `notElem` sup) $
         changeProperty32 dpy r a_NET_SUPPORTED a propModeAppend [fromIntegral x]

addEWMHFullscreen :: X ()
addEWMHFullscreen   = do
    wms <- getAtom "_NET_WM_STATE"
    wfs <- getAtom "_NET_WM_STATE_FULLSCREEN"
    mapM_ addNETSupported [wms, wfs]

------------------------------------------------------------------------
-- Key bindings. Add, modify or remove key bindings here.
--

warpToCenter = gets (W.screen . W.current . windowset) >>= \x -> warpToScreen x  0.5 0.5

myKeys conf@(XConfig {XMonad.modMask = modm}) = M.fromList $
    [ ((modm,               xK_Return), spawn $ XMonad.terminal conf)
    , ((modm,               xK_l)     , sendMessage $ Go R)
    , ((modm,               xK_h)     , sendMessage $ Go L)
    , ((modm,               xK_k)     , sendMessage $ Go U)
    , ((modm,               xK_j)     , sendMessage $ Go D)
    , ((modm .|. shiftMask, xK_l)     , sendMessage $ Swap R)
    , ((modm .|. shiftMask, xK_h)     , sendMessage $ Swap L)
    , ((modm .|. shiftMask, xK_k)     , sendMessage $ Swap U)
    , ((modm .|. shiftMask, xK_j)     , sendMessage $ Swap D)
    , ((modm,               xK_d)     , spawn "~/.config/rofi/launchers/colorful/launcher.sh")
    , ((modm,               xK_semicolon)     , spawn "emoji-picker")

    -- -- Audio keys
    -- , ((0,                    xF86XK_AudioPlay), spawn "playerctl play-pause")
    -- , ((0,                    xF86XK_AudioPrev), spawn "playerctl previous")
    -- , ((0,                    xF86XK_AudioNext), spawn "playerctl next")
    -- , ((0,                    xF86XK_AudioRaiseVolume), spawn "pactl set-sink-volume 0 +5%")
    -- , ((0,                    xF86XK_AudioLowerVolume), spawn "pactl set-sink-volume 0 -5%")
    -- , ((0,                    xF86XK_AudioMute), spawn "pactl set-sink-mute 0 toggle")

    -- -- Brightness keys
    -- , ((0,                    xF86XK_MonBrightnessUp), spawn "brightnessctl s +10%")
    -- , ((0,                    xF86XK_MonBrightnessDown), spawn "brightnessctl s 10-%")

    -- Screenshot
    , ((modm .|. shiftMask, xK_s), spawn "flameshot gui")

    -- Resize
    , ((modm .|. controlMask, xK_l     ), sendMessage Expand)
    , ((modm .|. controlMask, xK_h     ), sendMessage Shrink)

    -- Fullscreen Current Pane
    , ((modm, xK_f), sendMessage (Toggle FULL))

    -- close focused window
    , ((modm, xK_q), kill)

    -- Toggle gaps
    , ((modm, xK_g), toggleScreenSpacingEnabled >> toggleWindowSpacingEnabled)

    -- Rotate through the available layout algorithms
    , ((modm,               xK_space ), sendMessage NextLayout)

    -- Move focus to the next window
    , ((modm,               xK_Tab   ), windows W.focusDown)

    -- Move focus to the master window
    , ((modm,               xK_m     ), windows W.focusMaster  )

    -- Swap the focused window and the master window
    , ((modm .|. shiftMask, xK_m), windows W.swapMaster)

    -- Push window back into tiling
    , ((modm,               xK_t     ), withFocused $ windows . W.sink)

    -- Increment the number of windows in the master area
    , ((modm              , xK_comma ), sendMessage (IncMasterN 1))

    -- Deincrement the number of windows in the master area
    , ((modm              , xK_period), sendMessage (IncMasterN (-1)))

    -- Quit xmonad
    , ((modm .|. shiftMask, xK_q), io (exitWith ExitSuccess))

    -- Restart xmonad
    , ((modm, xK_c), spawn "xmonad --recompile; xmonad --restart")
    ]
    ++
    --
    -- mod-<workspace-key>, Switch to workspace
    -- mod-shift-<workspace-key>, Move client to workspace
    --
    [((m .|. modm, k), windows $ onCurrentScreen f i)
        | (i, k) <- zip (workspaces' conf) myWorkspaceKeys
        , (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]]
    ++

    --
    -- mod-{w,e,r}, Switch to physical/Xinerama screens 1, 2, or 3
    -- mod-shift-{w,e,r}, Move client to screen 1, 2, or 3
    --
    [((m .|. modm, key), screenWorkspace sc >>= flip whenJust (windows . f) >> warpToCenter)
        | (key, sc) <- zip [xK_w, xK_e, xK_r] [2, 1, 0]
        , (f, m) <- [(W.view, 0), (W.shift, shiftMask)]]


------------------------------------------------------------------------
-- Mouse bindings: default actions bound to mouse events
--
myMouseBindings (XConfig {XMonad.modMask = modm}) = M.fromList $

    -- mod-button1, Set the window to floating mode and move by dragging
    [ ((modm, button1), (\w -> focus w >> mouseMoveWindow w
                                       >> windows W.shiftMaster))

    -- mod-button2, Raise the window to the top of the stack
    , ((modm, button2), (\w -> focus w >> windows W.shiftMaster))

    -- mod-button3, Set the window to floating mode and resize by dragging
    , ((modm, button3), (\w -> focus w >> mouseResizeWindow w
                                       >> windows W.shiftMaster))

    -- you may also bind events to the mouse scroll wheel (button4 and button5)
    ]

------------------------------------------------------------------------
-- Layouts:

-- You can specify and transform your layouts by modifying these values.
-- If you change layout bindings be sure to use 'mod-shift-space' after
-- restarting (with 'mod-q') to reset your layout state to the new
-- defaults, as xmonad preserves your old layout settings by default.
--
-- The available layouts.  Note that each layout is separated by |||,
-- which denotes layout choice.
--
myLayout = windowNavigation $ mkToggle (NOBORDERS ?? FULL ?? EOT) $
    tiled
    ||| Mirror tiled
    ||| Full
  where
     -- default tiling algorithm partitions the screen into two panes
     tiled   = Tall nmaster delta ratio

     -- The default number of windows in the master pane
     nmaster = 1

     -- Default proportion of screen occupied by master pane
     ratio   = 1/2

     -- Percent of screen to increment by when resizing panes
     delta   = 3/100

------------------------------------------------------------------------
-- Window rules:

-- Execute arbitrary actions and WindowSet manipulations when managing
-- a new window. You can use this to, for example, always float a
-- particular program, or have a client always appear on a particular
-- workspace.
--
-- To find the property name associated with a program, use
-- > xprop | grep WM_CLASS
-- and click on the client you're interested in.
--
-- To match on the WM_NAME, you can use 'title' in the same way that
-- 'className' and 'resource' are used below.
--
myManageHook = manageZoomHook <+> fullscreenManageHook <+> manageDocks <+> composeAll
    [ className =? "MPlayer"        --> doFloat
    , className =? "Gimp"           --> doFloat
    , resource  =? "desktop_window" --> doIgnore
    , resource  =? "kdesktop"       --> doIgnore
    , isFullscreen --> doFullFloat
    ]

manageZoomHook =
  composeAll $
    [ (className =? zoomClassName) <&&> shouldFloat <$> title --> doFloat,
      (className =? zoomClassName) <&&> shouldSink <$> title --> doSink
    ]
  where
    zoomClassName = "zoom"
    tileTitles =
      [ "Zoom - Free Account" -- main window,
      , "Zoom - Licensed Account" -- main window,
      , "Zoom" -- meeting window on creation,
      , "Zoom Meeting" -- meeting window shortly after creation
      ]
    shouldFloat title = title `notElem` tileTitles
    shouldSink title = title `elem` tileTitles
    doSink = (ask >>= doF . W.sink) <+> doF W.swapDown


------------------------------------------------------------------------
-- Event handling

-- * EwmhDesktops users should change this to ewmhDesktopsEventHook
--
-- Defines a custom handler function for X Events. The function should
-- return (All True) if the default handler is to be run afterwards. To
-- combine event hooks use mappend or mconcat from Data.Monoid.
--
myEventHook =
  mconcat
    [ dynamicTitle manageZoomHook,
      handleEventHook def
    ]

------------------------------------------------------------------------
-- Startup hook

-- Perform an arbitrary action each time xmonad starts or is restarted
-- with mod-q.  Used by, e.g., XMonad.Layout.PerWorkspace to initialize
-- per-workspace layout choices.
--
-- By default, do nothing.
myStartupHook = do
  spawn "xsetroot -cursor_name left_ptr"
  spawnOnce "nitrogen --restore"
  spawnOnce "picom -f"
  spawnOnce "xsetwacom set 11 MapToOutput 1920x1080+0+200"
  spawnOnce "xsetwacom set 11 Rotate half"
  spawnOnce "redshift -l 35.4:-97.5 -r"
  spawnOnce "~/bin/xmonad_fix.sh"
  spawnOnce "yubikey-touch-detector -libnotify"


------------------------------------------------------------------------
-- Now run xmonad with all the defaults we set up.

-- Run xmonad with the settings you specify. No need to modify this.
--
main = do
    nScreens <- countScreens
    hs <- mapM (spawnPipe . xmobarCommand) [0..nScreens-1]
    xmonad $ fullscreenSupport $ docks $ ewmh $ defaults hs nScreens

xmobarCommand (S s) =
    "xmobar ~/.config/xmobar -x " ++ (show s)

-- A structure containing your configuration settings, overriding
-- fields in the default config. Any you don't override, will
-- use the defaults defined in xmonad/XMonad/Config.hs
--
-- No need to modify this.
--
defaults hs nScreens = def {
        terminal           = myTerminal,
        focusFollowsMouse  = myFocusFollowsMouse,
        clickJustFocuses   = myClickJustFocuses,
        borderWidth        = myBorderWidth,
        modMask            = myModMask,
        workspaces         = withScreens nScreens myWorkspaces,
        normalBorderColor  = myNormalBorderColor,
        focusedBorderColor = myFocusedBorderColor,

        keys          = myKeys,
        mouseBindings = myMouseBindings,

        manageHook      = myManageHook,
        layoutHook      = avoidStruts $ spacingRaw False (Border 10 10 10 10) True (Border 10 10 10 10) True $ myLayout,
        handleEventHook = myEventHook,
        logHook         = mapM_ dynamicLogWithPP $ zipWith pp hs [0..nScreens],
        startupHook     = myStartupHook >> addEWMHFullscreen
    }

pp h s =
    marshallPP s xmobarPP
        { ppOutput            = hPutStrLn h
        , ppCurrent           = color "#00ff00"
        , ppVisible           = color "#888888"
        , ppHidden           = color "#1b9fde"
        , ppHiddenNoWindows   = color "#888888"
        , ppWsSep            = "      "
        , ppSep = "    |    "
        , ppTitle = color "#bbbbbb"
        , ppLayout  = (\layout -> case layout of
                   "Spacing Tall" -> "\xeb56"
                   "Spacing Full" -> "\xf08c6"
                   "Spacing Mirror Tall" -> "\xeb57"
                   _ -> layout
                   )
        }
    where color c = xmobarColor c ""
