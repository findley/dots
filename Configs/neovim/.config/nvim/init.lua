require("opts") -- basic options
require("keys") -- basic keymaps (plugin specific keymaps are in setup functions)
require("color") -- setup colorscheme
require("yank-highlight") -- autocmd to highlight after yank
require("util")

BootstrapPackageManager() -- See util for details

--  To check the current status of your plugins, run
--    :Lazy
--
--  You can press `?` in this menu for help. Use `:q` to close the window
--
--  To update plugins, you can run
--    :Lazy update
--
--  All files in lua/plugins/ are merged into the lazy config
--  Simple plugins are in lua/plugins/init.lua, while plugins with more sophisticated
--  configuration are in their own files.
require("lazy").setup("plugins")
