highlight @md_unch guifg=#F8F8F2
highlight @text.todo.unchecked guifg=#F8F8F2

highlight @md_ch guifg=#375749 gui=strikethrough
highlight @text.todo.checked guifg=#375749

highlight @markup.heading.1.markdown guifg=#aaaaff gui=bold
highlight @markup.heading.2.markdown guifg=#33cc33 gui=bold

highlight @markup.list.unchecked.markdown guifg=#9999cc gui=bold
