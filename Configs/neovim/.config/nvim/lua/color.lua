vim.cmd.colorscheme("focuspoint_mod")

-- Highlgiht Trailing Whitespace
-- vim.cmd.hi("ExtraWhitespace ctermbg=red guibg=#6c1c1c")
-- vim.cmd([[:match ExtraWhitespace /\s\+$/]])

-- Nicer diagnostic signs
local signs = { Error = "", Warn = "", Hint = "", Info = " " }
for type, icon in pairs(signs) do
  local hl = "DiagnosticSign" .. type
  vim.fn.sign_define(hl, { text = icon, texthl = hl, numhl = hl })
end
