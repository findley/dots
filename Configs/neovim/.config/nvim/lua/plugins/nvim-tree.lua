return {
  "kyazdani42/nvim-tree.lua",
  config = function()
    require("nvim-tree").setup({
      sort_by = "case_sensitive",
      view = {
        adaptive_size = true,
      },
      renderer = {
        group_empty = true,
      },
      filters = {
        dotfiles = false,
        git_ignored = false,
      },
    })

    -- Open File Tree
    vim.keymap.set('n', '<leader>e', '<cmd>NvimTreeFindFileToggle<CR>', { noremap = true })
  end
}
