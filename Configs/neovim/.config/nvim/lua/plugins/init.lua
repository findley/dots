return {
  -- Detect tabstop and shiftwidth automatically
  "tpope/vim-sleuth",

  -- Quickly comment and uncomment code with gc
  { "numToStr/Comment.nvim",    opts = {} },

  -- Highlight todo, notes, etc in comments
  { 'folke/todo-comments.nvim', event = 'VimEnter', dependencies = { 'nvim-lua/plenary.nvim' }, opts = {} },
}
