return {
  -- See `:help gitsigns` to understand what the configuration keys do
  { -- Adds git related signs to the gutter, as well as utilities for managing changes
    "lewis6991/gitsigns.nvim",
    config = function()
      require("gitsigns").setup({
        signs = {
          add          = { text = '█' },
          change       = { text = '█' },
          delete       = { text = '█' },
          topdelete    = { text = '█' },
          changedelete = { text = '█' },
          untracked    = { text = '┆' },
        },
      })
      vim.keymap.set('n', '<leader>gd', function() require("gitsigns").preview_hunk_inline() end,
        { noremap = true, desc = "git diff current hunk" })
    end
  },

  -- git integratgion (I mostly just use gitblame)
  {
    "tpope/vim-fugitive",
    dependencies = {
      "tpope/vim-rhubarb",
    },
    config = function()
      -- Run Fugitive Git Blame (,gb)
      vim.keymap.set('n', '<leader>gb', '<cmd>G blame<CR>', { noremap = true, desc = "git blame current file" })
    end
  },
}
