-- Markdown wiki support
return {
  "lervag/wiki.vim",
  config = function()
    vim.g.wiki_root = '~/zk'
    vim.g.wiki_mappings_use_defaults = 'local'
    vim.g.wiki_filetypes = { 'md', 'wiki', 'jrnl' }
    vim.g.wiki_link_target_type = 'md'
    vim.g.wiki_link_extension = '.md'
    vim.g.wiki_write_on_nav = 1
    vim.cmd([[autocmd FileType markdown setlocal textwidth=80]])
    vim.cmd([[autocmd FileType markdown iab <expr> jdt strftime("%b %d, %Y")]])
  end
}
