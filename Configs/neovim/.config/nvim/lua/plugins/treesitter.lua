-- Highlight, edit, and navigate code
return {
  "nvim-treesitter/nvim-treesitter",
  build = ":TSUpdate",
  dependencies = {
    "nvim-treesitter/nvim-treesitter-context",
  },
  config = function()
    -- [[ Configure Treesitter ]] See `:help nvim-treesitter`

    ---@diagnostic disable-next-line: missing-fields
    require("nvim-treesitter.configs").setup({
      ensure_installed = {
        "bash",
        "c",
        "html",
        "lua",
        "markdown",
        "vim",
        "vimdoc",
        "go",
        "rust",
        "javascript",
        "typescript",
        "dockerfile",
        "scala",
      },
      -- Autoinstall languages that are not installed
      auto_install = false,
      highlight = {
        enable = true,
        additional_vim_regex_highlighting = true,
     },
      indent = { enable = true },
    })

    require("treesitter-context").setup({})
  end,
}
