-- Useful plugin to show you pending keybinds.
return {
  "folke/which-key.nvim",
  event = "VimEnter",   -- Sets the loading event to 'VimEnter'
  cond = false, -- This plugin is pretty annoying, disable for now, might remove later.
  config = function()   -- This is the function that runs, AFTER loading
    require("which-key").setup({
      plugins = {
        registers = false,
        marks = false,
      },
    })

    -- Document existing key chains
    require("which-key").register({
      ["<leader>c"] = { name = "[C]ode", _ = "which_key_ignore" },
      ["<leader>d"] = { name = "[D]ocument", _ = "which_key_ignore" },
      ["<leader>r"] = { name = "[R]ename", _ = "which_key_ignore" },
      ["<leader>w"] = { name = "[W]orkspace", _ = "which_key_ignore" },
    })
  end,
}
