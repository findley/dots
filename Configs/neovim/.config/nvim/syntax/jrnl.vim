if exists("b:current_syntax")
    finish
endif

let b:current_syntax = "jrnl"

syntax match DateLabel '^Date:'
syntax match SubjectLabel '^Subject:'
syntax match DurationLabel '^Duration:'
syntax match IssueNumber '\v![A-Za-z\-0-9]+'
syntax match Mention '\v\@[A-Za-z.]+'
syntax match HashTag '\v#[A-Za-z.\-_0-9]+'

hi def link IssueNumber Constant
hi def link Mention Type
hi def link HashTag Statement
hi def link DateLabel PreProc
hi def link SubjectLabel PreProc
hi def link DurationLabel PreProc
