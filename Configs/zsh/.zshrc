# Enable Powerlevel10k instant prompt. Should stay close to the top of ~/.zshrc.
# Initialization code that may require console input (password prompts, [y/n]
# confirmations, etc.) must go above this block; everything else may go below.
if [[ -r "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh" ]]; then
  source "${XDG_CACHE_HOME:-$HOME/.cache}/p10k-instant-prompt-${(%):-%n}.zsh"
fi

HISTFILE=~/.histfile
HISTSIZE=100000000
SAVEHIST=100000000

setopt HIST_IGNORE_ALL_DUPS
setopt APPEND_HISTORY
setopt SHARE_HISTORY

# Lines configured by zsh-newuser-install
bindkey -v

zstyle :compinstall filename '/home/david/.zshrc'
autoload -Uz compinit
compinit

# P10K Theme
source ~/.config/powerlevel10k/powerlevel10k.zsh-theme

# Better vi mode
function zvm_config() {
  ZVM_LINE_INIT_MODE=$ZVM_MODE_INSERT
}
source $HOME/.config/zsh-vi-mode/zsh-vi-mode.plugin.zsh
zvm_after_init_commands+=('[ -f ~/.config/fzf-key-bindings.zsh ] && source ~/.config/fzf-key-bindings.zsh')

# Exports
export EDITOR=nvim

if [ -d "$HOME/bin" ] ; then
    PATH="$HOME/bin:$PATH"
fi

if [ -d "$HOME/.config/shared-bin" ] ; then
    PATH="$HOME/.config/shared-bin:$PATH"
fi

if [ -d "$HOME/.local/bin" ] ; then
    PATH="$HOME/.local/bin:$PATH"
fi

if [ -d "$HOME/.npm-global/bin" ] ; then
    PATH="$HOME/.npm-global/bin:$PATH"
fi

if [ -d "$HOME/.yarn/bin" ] ; then
    PATH="$HOME/.yarn/bin:$PATH"
fi

if [ -d "$HOME/go/bin" ] ; then
    PATH="$HOME/go/bin:$PATH"
fi

if [ -d "$HOME/.cargo/bin" ] ; then
    PATH="$HOME/.cargo/bin:$PATH"
fi

# Aliases
alias ls="ls --color"
alias dots='git --git-dir=$HOME/.dots --work-tree=$HOME'
if type nvim &>/dev/null ; then
    alias vim='nvim'
fi
alias zk='vim ~/zk/index.md'
alias zkstat="git --git-dir ~/zk/.git --work-tree ~/zk status"
alias zkdiff="git --git-dir ~/zk/.git --work-tree ~/zk diff"
alias xo='xdg-open'
alias sk='screenkey --position fixed -s small --opacity 0.3 --geometry `slop`'

# To customize prompt, run `p10k configure` or edit ~/.p10k.zsh.
[[ ! -f ~/.p10k.zsh ]] || source ~/.p10k.zsh

# Normal backspace behaviour
bindkey "^?" backward-delete-char

# Allow Ctrl-z to toggle between suspend and resume
function Resume {
    fg
    zle push-input
    BUFFER=""
    zle accept-line
}
zle -N Resume
bindkey "^Z" Resume

# zoxide
eval "$(zoxide init zsh --cmd cd)"

# pyenv
export PYENV_ROOT="$HOME/.pyenv"
[[ -d $PYENV_ROOT/bin ]] && export PATH="$PYENV_ROOT/bin:$PATH"
eval "$(pyenv init -)"

# bun completions
[ -s "/home/david/.bun/_bun" ] && source "/home/david/.bun/_bun"

# bun
export BUN_INSTALL="$HOME/.bun"
export PATH="$BUN_INSTALL/bin:$PATH"

# fnm
FNM_PATH="/home/david/.local/share/fnm"
if [ -d "$FNM_PATH" ]; then
  export PATH="/home/david/.local/share/fnm:$PATH"
  eval "`fnm env`"
fi

# BEGIN opam configuration
[[ ! -r '/home/david/.opam/opam-init/init.zsh' ]] || source '/home/david/.opam/opam-init/init.zsh' > /dev/null 2> /dev/null
# END opam configuration
#

# >>> coursier install directory >>>
export PATH="$PATH:/home/david/.local/share/coursier/bin"
# <<< coursier install directory <<<
