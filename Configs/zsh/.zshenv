export GOPATH=$HOME/go
export PATH=$GOPATH/bin:$GOROOT/bin:$PATH
export GPG_TTY=$(tty)
export KUBECONFIG=$HOME/.kube/geared.yaml:$HOME/.kube/muse.yaml

# This is a hack to allow newer node version on old webpack codebase
# export NODE_OPTIONS=--openssl-legacy-provider
