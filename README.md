# Dotfiles

Dotfiles are stored in directories inside `Configs/` grouped mainly by application.

## Setup

To install these dotfiles, first clone this repo into `~/.config/dotfiles`, then
use [tuckr](https://github.com/RaphGL/Tuckr) to install configs for the desired
applications.

```
$ cargo install --git https://github.com/RaphGL/Tuckr.git
$ git clone git@gitlab.com:findley/dots.git ~/.config/dotfiles
$ tuckr add zsh
$ tuckr add git
$ tuckr add neovim
...
$ tuckr status
```

## Screenshots
![Desktop Screenshot](/desktop/.config/2022-07-31_desktop.png "Desktop")
